-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 13 Mar 2023 pada 11.29
-- Versi server: 5.7.33
-- Versi PHP: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spion`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `kompetensi_keahlian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`, `kompetensi_keahlian`) VALUES
(1, 'XII', 'Rekasaya Perang Lunak'),
(4, 'XI', 'Multimedia'),
(13, 'XIII', 'RAI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `tahun_ajaran` varchar(9) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `tahun_ajaran`, `nominal`) VALUES
(1, '2021', 1500000),
(2, '2022', 1700000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `password`, `role`) VALUES
(1, 'admin', 'admin', 1),
(2, 'petugas', 'petugas', 2),
(3, 'danta', 'danta', 3),
(4, 'rio', 'rio', 3),
(5, 'restu', 'restu', 3),
(6, 'aneh', 'aneh', 1),
(7, 'adikonen', 'adikonen', 1),
(9, 'restu', 'restu', 3),
(10, 'hh', 'hh', 3),
(11, 'test', 'test', 1),
(12, 'adam', 'adam', 1),
(13, 'tai', 'tai', 1),
(14, 'tata', 'tatat', 1),
(15, 'bola', 'bola', 1),
(16, 'sadfsa', 'assa', 1),
(17, 'adam', 'adam', 1),
(19, 'restu', 'restu', 1),
(21, 'sac', 'saca', 3),
(23, 'patrick new455', 'patrick455', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_pengguna` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `id_pengguna`) VALUES
(1, 'administrator', 1),
(2, 'petugas', 2),
(3, 'adi konen anjay', 7),
(5, 'damdambola', 17),
(7, 'anjay', 19);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nisn` varchar(24) NOT NULL,
  `nis` char(6) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(24) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nisn`, `nis`, `nama`, `alamat`, `telepon`, `id_kelas`, `id_pengguna`, `id_pembayaran`) VALUES
(1, '000000', '28893', 'danta', 'jalan sulatri no 14 kesiman petilan denpasar', '087784793721', 1, 3, 2),
(2, '28892', '28892', 'rio renata', 'jalan gumitir', '00', 1, 4, 1),
(4, '44', '44', 'hh', 'hh', '44', 1, 10, 2),
(6, '455', '455', 'patrick455', 'bikini bottom44', '444', 13, 23, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tanggal_bayar` date NOT NULL,
  `bulan_bayar` int(11) NOT NULL,
  `tahun_bayar` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal_bayar`, `bulan_bayar`, `tahun_bayar`, `id_siswa`, `id_petugas`, `id_pembayaran`) VALUES
(1, '2023-03-12', 1, 2023, 1, 1, 2),
(2, '2023-03-12', 2, 2023, 1, 1, 2),
(3, '2023-03-12', 3, 2023, 1, 1, 2),
(4, '2023-03-12', 1, 2023, 6, 1, 2),
(5, '2023-03-12', 2, 2023, 6, 1, 2),
(6, '2023-03-12', 1, 2023, 4, 1, 2),
(7, '2023-03-12', 2, 2023, 4, 1, 2),
(8, '2023-03-12', 3, 2023, 4, 1, 2);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_pengguna`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_pengguna` (
`id_pengguna` int(11)
,`username` varchar(255)
,`password` varchar(255)
,`role` int(11)
,`id_siswa` int(11)
,`id_petugas` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_petugas`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_petugas` (
`id_petugas` int(11)
,`nama` varchar(50)
,`id_pengguna` int(11)
,`role` int(11)
,`username` varchar(255)
,`password` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_siswa`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_siswa` (
`id_siswa` int(11)
,`nisn` varchar(24)
,`nis` char(6)
,`nama` varchar(255)
,`alamat` text
,`telepon` varchar(24)
,`id_kelas` int(11)
,`id_pengguna` int(11)
,`id_pembayaran` int(11)
,`kelas` varchar(255)
,`kompetensi_keahlian` varchar(255)
,`username` varchar(255)
,`password` varchar(255)
,`role` int(11)
,`tahun_ajaran` varchar(9)
,`nominal` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_transaksi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_transaksi` (
`tanggal_bayar` date
,`bulan_bayar` int(11)
,`tahun_bayar` int(11)
,`id_siswa` int(11)
,`id_transaksi` int(11)
,`nisn` varchar(24)
,`nis` char(6)
,`nama` varchar(255)
,`alamat` text
,`telepon` varchar(24)
,`tahun_ajaran` varchar(9)
,`nominal` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_pengguna`
--
DROP TABLE IF EXISTS `v_pengguna`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pengguna`  AS SELECT `pengguna`.`id_pengguna` AS `id_pengguna`, `pengguna`.`username` AS `username`, `pengguna`.`password` AS `password`, `pengguna`.`role` AS `role`, `siswa`.`id_siswa` AS `id_siswa`, `petugas`.`id_petugas` AS `id_petugas` FROM ((`pengguna` left join `siswa` on((`pengguna`.`id_pengguna` = `siswa`.`id_pengguna`))) left join `petugas` on((`pengguna`.`id_pengguna` = `petugas`.`id_pengguna`)))  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_petugas`
--
DROP TABLE IF EXISTS `v_petugas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_petugas`  AS SELECT `petugas`.`id_petugas` AS `id_petugas`, `petugas`.`nama` AS `nama`, `petugas`.`id_pengguna` AS `id_pengguna`, `pengguna`.`role` AS `role`, `pengguna`.`username` AS `username`, `pengguna`.`password` AS `password` FROM (`petugas` join `pengguna` on((`petugas`.`id_pengguna` = `pengguna`.`id_pengguna`)))  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_siswa`
--
DROP TABLE IF EXISTS `v_siswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_siswa`  AS SELECT `siswa`.`id_siswa` AS `id_siswa`, `siswa`.`nisn` AS `nisn`, `siswa`.`nis` AS `nis`, `siswa`.`nama` AS `nama`, `siswa`.`alamat` AS `alamat`, `siswa`.`telepon` AS `telepon`, `siswa`.`id_kelas` AS `id_kelas`, `siswa`.`id_pengguna` AS `id_pengguna`, `siswa`.`id_pembayaran` AS `id_pembayaran`, `kelas`.`kelas` AS `kelas`, `kelas`.`kompetensi_keahlian` AS `kompetensi_keahlian`, `pengguna`.`username` AS `username`, `pengguna`.`password` AS `password`, `pengguna`.`role` AS `role`, `pembayaran`.`tahun_ajaran` AS `tahun_ajaran`, `pembayaran`.`nominal` AS `nominal` FROM (((`siswa` left join `kelas` on((`siswa`.`id_kelas` = `kelas`.`id_kelas`))) left join `pengguna` on((`siswa`.`id_pengguna` = `pengguna`.`id_pengguna`))) left join `pembayaran` on((`siswa`.`id_pembayaran` = `pembayaran`.`id_pembayaran`)))  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_transaksi`
--
DROP TABLE IF EXISTS `v_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_transaksi`  AS SELECT `transaksi`.`tanggal_bayar` AS `tanggal_bayar`, `transaksi`.`bulan_bayar` AS `bulan_bayar`, `transaksi`.`tahun_bayar` AS `tahun_bayar`, `siswa`.`id_siswa` AS `id_siswa`, `transaksi`.`id_transaksi` AS `id_transaksi`, `siswa`.`nisn` AS `nisn`, `siswa`.`nis` AS `nis`, `siswa`.`nama` AS `nama`, `siswa`.`alamat` AS `alamat`, `siswa`.`telepon` AS `telepon`, `pembayaran`.`tahun_ajaran` AS `tahun_ajaran`, `pembayaran`.`nominal` AS `nominal` FROM ((`transaksi` left join `siswa` on((`transaksi`.`id_siswa` = `siswa`.`id_siswa`))) left join `pembayaran` on((`transaksi`.`id_pembayaran` = `pembayaran`.`id_pembayaran`)))  ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_pengguna` (`id_pengguna`),
  ADD KEY `id_pembayaran` (`id_pembayaran`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_pembayaran` (`id_pembayaran`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_pembayaran`) REFERENCES `pembayaran` (`id_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_3` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id_siswa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_pembayaran`) REFERENCES `pembayaran` (`id_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
