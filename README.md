# syntax

create view pengguna:

create view v_pengguna as select pengguna.* ,petugas.id_petugas,siswa.id_siswa from pengguna left join petugas on pengguna.id_pengguna = petugas.id_pengguna left join siswa on pengguna.id_pengguna = siswa.id_pengguna 

create view petugas:
create view v_petugas as select petugas.*, pengguna.username,pengguna.password,pengguna.role from petugas left join pengguna on petugas.id_pengguna = pengguna.id_pengguna

create view siswa:
create view v_siswa as select siswa.* ,pengguna.username,pengguna.password,pengguna.role,kelas.kelas,kelas.kompetensi_keahlian,pembayaran.tahun_ajaran,pembayaran.nominal from siswa left join pengguna on siswa.id_pengguna = pengguna.id_pengguna left join kelas on siswa.id_kelas = kelas.id_kelas left join pembayaran on siswa.id_pembayaran = pembayaran.id_pembayaran

create view transaksi :
create view v_transaksi as select transaksi.* ,siswa.nisn,siswa.nama,kelas.kelas,kelas.kompetensi_keahlian,petugas.nama,pembayaran.nominal from transaksi left join siswa on transaksi.id_siswa = siswa.id_siswa left join kelas on siswa.id_kelas = kelas.id_kelas left join petugas on transaksi.id_petugas = petugas.id_petugas left join pembayaran on transaksi.id_pembayaran = pembayaran.id_pembayaran

create sstore procedure kelas
delimiter //
create store procedure selectAllKelas()
begin
select * from kelas;
end //

cara manggil 
myasqli_query($conn,'call selectAllkelas()');