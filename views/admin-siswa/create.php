<?php
include '../../functions/siswa.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$siswa = new Siswa();

if (isset($_POST['submit'])) {

    if ($siswa->create($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

$kelas = mysqli_query($conn, "SELECT * FROM kelas");
$pembayaran = mysqli_query($conn, "SELECT * FROM pembayaran");

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <form method="post">
        <div class="form-group">
            <label for="username">username</label>
            <input type="text" name="username" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">password</label>
            <input type="text" name="password" class="form-control">
        </div>
        <input type="hidden" name="role" value="3">
        <div class="form-group">
            <label for="nis">nis</label>
            <input type="text" name="nis" class="form-control">
        </div>
        <div class="form-group">
            <label for="nisn">nisn</label>
            <input type="text" name="nisn" class="form-control">
        </div>
        <div class="form-group">
            <label for="nama">nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        <div class="form-group">
            <label for="alamat">alamat</label>
            <input type="text" name="alamat" class="form-control">
        </div>
        <div class="form-group">
            <label for="telepon">telepon</label>
            <input type="text" name="telepon" class="form-control">
        </div>

        <div class="form-group">
            <label for="kelas">kelas</label>
            <select name="id_kelas" class="form-control">
                <?php foreach ($kelas as $row) : ?>
                    <option value="<?= $row['id_kelas'] ?>"><?= $row['kelas'] ?> - <?= $row['kompetensi_keahlian'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="kelas">kelas</label>
            <select name="id_pembayaran" class="form-control">
                <?php foreach ($pembayaran as $row) : ?>
                    <option value="<?= $row['id_pembayaran'] ?>"><?= $row['tahun_ajaran'] ?> - <?= $row['nominal'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
    </form>
</div>
<?php include '../templates/footer.php' ?>