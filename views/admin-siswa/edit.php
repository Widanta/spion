<?php
include '../../functions/siswa.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$siswa = new Siswa();

if (isset($_POST['submit'])) {
    if ($siswa->edit($_POST) > 0) {
        echo "<script>alert('Data berhasil diubah');window.location.href='index.php'</script>";
    }
}

$siswa = mysqli_query($conn, "SELECT * FROM siswa left join pengguna on siswa.id_pengguna = pengguna.id_pengguna WHERE id_siswa = '$_GET[id_siswa]'");
$kelas = mysqli_query($conn, "SELECT * FROM kelas");
$pembayaran = mysqli_query($conn, "SELECT * FROM pembayaran");


?>

<?php include '../templates/header.php' ?>
<div class="row">

    <?php foreach ($siswa as $row) : ?>
        <form method="post">
            <input type="hidden" name="id_siswa" value="<?= $row['id_siswa']; ?>">
            <input type="hidden" name="id_pengguna" value="<?= $row['id_pengguna']; ?>">
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" name="username" class="form-control" value="<?= $row['username']; ?>">
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="text" name="password" class="form-control" value="<?= $row['password']; ?>">
            </div>
            <input type="hidden" name="role" value="3">
            <div class="form-group">
                <label for="nis">nis</label>
                <input type="text" name="nis" class="form-control" value="<?= $row['nis']; ?>">
            </div>
            <div class="form-group">
                <label for="nisn">nisn</label>
                <input type="text" name="nisn" class="form-control" value="<?= $row['nisn']; ?>">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" name="nama" class="form-control" value="<?= $row['nama']; ?>">
            </div>
            <div class="form-group">
                <label for="alamat">alamat</label>
                <input type="text" name="alamat" class="form-control" value="<?= $row['alamat']; ?>">
            </div>
            <div class="form-group">
                <label for="telepon">telepon</label>
                <input type="text" name="telepon" class="form-control" value="<?= $row['telepon']; ?>">
            </div>

            <div class="form-group">
                <label for="kelas">kelas</label>
                <select name="id_kelas" class="form-control">
                    <option value="<?= $row['id_kelas']; ?>">pilihan sebelumnya <?= $row['id_kelas']; ?></option>
                    <?php foreach ($kelas as $row) : ?>
                        <option value="<?= $row['id_kelas'] ?>"><?= $row['kelas'] ?> - <?= $row['kompetensi_keahlian'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="kelas">pembayaran</label>
                <select name="id_pembayaran" class="form-control">
                    <?php foreach ($pembayaran as $row) : ?>
                        <option value="<?= $row['id_pembayaran'] ?>"><?= $row['tahun_ajaran'] ?> - <?= $row['nominal'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

        <?php endforeach; ?>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
        </form>
</div>
<?php include '../templates/footer.php' ?>