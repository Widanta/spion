<?php
include '../../functions/connect.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$kelas = mysqli_query($conn, "call selectAllKelas()");


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <tr>
                <th>No</th>
                <th>Kelas</th>
                <th>Kompetensi Keahlian</th>
            </tr>
            <?php foreach ($kelas as $row) : ?>
                <tr>
                    <td>
                        <a href="edit.php?id_kelas=<?= $row['id_kelas'] ?>" class="btn"><i class="fas fa-pen text-primary"></i></a>
                        <a href="delete.php?id_kelas=<?= $row['id_kelas'] ?>" class="btn"><i class="fas fa-trash text-danger"></i></a>
                    </td>
                    <td><?= $row['kelas'] ?></td>
                    <td><?= $row['kompetensi_keahlian'] ?></td>
                <?php endforeach; ?>
        </table>
    </div>
</div>
<?php include '../templates/footer.php' ?>