<?php
include '../../functions/kelas.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$kelas = new Kelas();

if (isset($_POST['submit'])) {
    if ($kelas->edit($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

$kelas = mysqli_query($conn, "SELECT * FROM kelas WHERE id_kelas = {$_GET['id_kelas']} ");
?>

<?php include '../templates/header.php' ?>
<div class="row">
    <form method="post">
        <?php foreach ($kelas as $row) : ?>
            <input type="hidden" name="id_kelas" value="<?= $row['id_kelas']; ?>">
            <div class="form-group">
                <label for="nama">nama Kelas</label>
                <input type="text" name="kelas" class="form-control" value="<?= $row['kelas']; ?>">
            </div>
            <div class="form-group">
                <label for="kompetensi_keahlian">kompetensi keahlian</label>
                <input type="text" name="kompetensi_keahlian" class="form-control" value="<?= $row['kompetensi_keahlian']; ?>">
            </div>
        <?php endforeach; ?>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
    </form>
</div>
<?php include '../templates/footer.php' ?>