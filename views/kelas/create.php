<?php
include '../../functions/kelas.php';
$kelas = new Kelas();

if (isset($_POST['submit'])) {
    if ($kelas->create($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <form method="post">
        <div class="form-group">
            <label for="nama">nama Kelas</label>
            <input type="text" name="kelas" class="form-control">
        </div>
        <div class="form-group">
            <label for="kompetensi_keahlian">kompetensi keahlian</label>
            <input type="text" name="kompetensi_keahlian" class="form-control">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
    </form>
</div>
<?php include '../templates/footer.php' ?>