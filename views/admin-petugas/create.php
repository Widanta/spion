<?php
include '../../functions/petugas.php';
$petugas = new Petugas();

if (isset($_POST['submit'])) {

    if ($petugas->create($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <form method="post">
        <div class="form-group">
            <label for="username">username</label>
            <input type="text" name="username" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">password</label>
            <input type="text" name="password" class="form-control">
        </div>
        <div class="form-group">
            <label for="role">role</label>
            <select name="role" id="role">
                <option value="1">admin</option>
                <option value="2">petugas</option>
            </select>
        </div>
        <div class="form-group">
            <label for="nama">nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
    </form>
</div>
<?php include '../templates/footer.php' ?>