<?php
include '../../functions/petugas.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$petugas = new Petugas();

if (isset($_POST['submit'])) {
    if ($petugas->edit($_POST) > 0) {
        echo "<script>alert('Data berhasil diubah');window.location.href='index.php'</script>";
    }
}

$petugas = mysqli_query($conn, "SELECT * FROM petugas left join pengguna on petugas.id_pengguna = pengguna.id_pengguna WHERE id_petugas = '$_GET[id_petugas]'");

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <?php foreach ($petugas as $row) : ?>
        <form method="post">
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" name="username" class="form-control" value="<?= $row['username']; ?>">
            </div>
            <div class="form-group">
                <label for="password">password</label>
                <input type="text" name="password" class="form-control" value="<?= $row['password']; ?>">
            </div>
            <div class="form-group">
                <label for="role">role</label>
                <option value="<?= $row['role']; ?>"><?= $row['role']; ?></option>
                <select name="role" id="role">
                    <option value="1">admin</option>
                    <option value="2">petugas</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" name="nama" class="form-control" value="<?= $row['nama']; ?>">
            </div>
            <input type="hidden" name="id_petugas" value="<?= $row['id_petugas']; ?>">
            <input type="hidden" name="id_pengguna" value="<?= $row['id_pengguna']; ?>">
        <?php endforeach; ?>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
        </form>
</div>
<?php include '../templates/footer.php' ?>