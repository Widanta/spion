<?php
include '../../functions/connect.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$petugas = mysqli_query($conn, "SELECT * FROM petugas left join pengguna on petugas.id_pengguna = pengguna.id_pengguna");


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <tr>
                <th>No</th>
                <th>username</th>
                <th>password</th>
                <th>role</th>
                <th>nama</th>
            </tr>
            <?php foreach ($petugas as $row) : ?>
                <tr>
                    <td>
                        <a href="edit.php?id_petugas=<?= $row['id_petugas'] ?>" class="btn"><i class="fas fa-pen text-primary"></i></a>
                        <a href="delete.php?id_pengguna=<?= $row['id_pengguna'] ?>" class="btn"><i class="fas fa-trash text-danger"></i></a>
                    </td>
                    <td><?= $row['username'] ?></td>
                    <td><?= $row['password'] ?></td>
                    <td><?= $row['role'] ?></td>
                    <td><?= $row['nama'] ?></td>
                <?php endforeach; ?>
        </table>
    </div>
</div>
<?php include '../templates/footer.php' ?>