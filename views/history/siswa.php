<?php
include '../../functions/connect.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$transaksi = mysqli_query($conn, "SELECT * FROM v_transaksi where id_siswa = '$_GET[id_siswa]'");


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <tr>
                <th>No</th>
                <th>Tanggal bayar</th>
                <th>Bulan bayar</th>
                <th>Nisn</th>
                <th>Nama</th>
                <th>Nominal</th>
                <th>Status</th>

            </tr>
            <?php $i = 1; ?>
            <?php foreach ($transaksi as $row) : ?>
                <tr>
                    <td>
                        <?= $i++; ?>
                    </td>
                    <td><?= $row['tanggal_bayar'] ?></td>
                    <td><?= $row['bulan_bayar'] ?></td>
                    <td><?= $row['nisn'] ?></td>
                    <td><?= $row['nama'] ?></td>
                    <td><?= $row['nominal'] ?></td>
                    <td>
                        <div class="badge badge-success">lunas</div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<?php include '../templates/footer.php' ?>