<?php
include '../../functions/connect.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$siswa = mysqli_query($conn, "SELECT * FROM v_siswa");


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <tr>
                <th>No</th>
                <th>username</th>
                <th>password</th>
                <th>role</th>
                <th>nis</th>
                <th>nisn</th>
                <th>nama</th>
                <th>alamat</th>
                <th>telepon</th>
                <th>kelas</th>
                <th>tahun ajaran</th>
                <th>id_pengguna</th>
            </tr>
            <?php foreach ($siswa as $row) : ?>
                <tr>
                    <td>
                        <a href="bayar.php?id_siswa=<?= $row['id_siswa'] ?>" class="btn"><i class="fas fa-dollar-sign text-success"></i></a>
                    </td>
                    <td><?= $row['username'] ?></td>
                    <td><?= $row['password'] ?></td>
                    <td><?= $row['role'] ?></td>
                    <td><?= $row['nis'] ?></td>
                    <td><?= $row['nisn'] ?></td>
                    <td><?= $row['nama'] ?></td>
                    <td><?= $row['alamat'] ?></td>
                    <td><?= $row['telepon'] ?></td>
                    <td><?= $row['kelas'] ?></td>
                    <td><?= $row['tahun_ajaran'] ?></td>
                    <td><?= $row['id_pengguna'] ?></td>
                <?php endforeach; ?>
        </table>
    </div>
</div>
<?php include '../templates/footer.php' ?>