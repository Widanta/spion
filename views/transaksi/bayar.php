<?php
include '../../functions/transaksi.php';
$transaksi = new Transaksi();
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$siswa = mysqli_query($conn, "SELECT * FROM v_siswa where id_siswa = '$_GET[id_siswa]'");
$dataBulan = mysqli_query($conn, "SELECT bulan_bayar from transaksi where id_siswa = '$_GET[id_siswa]'");

$data['data_bulan'] = [
    'januari' => ['januari', 1],
    'februari' => ['februari', 2],
    'maret' => ['maret', 3],
    'april' => ['april', 4],
    'mei' => ['mei', 5],
    'juni' => ['juni', 6],
    'juli' => ['juli', 7],
    'agustus' => ['agustus', 8],
    'september' => ['september', 9],
    'oktober' => ['oktober', 10],
    'november' => ['november', 11],
    'desember' => ['desember', 12],
];
$bulan_dibayar = [];
foreach ($dataBulan as $bulan) {
    array_push($bulan_dibayar, $bulan['bulan_bayar']);
}
$data['bulan_bayar'] = $bulan_dibayar;



if (isset($_POST['submit'])) {

    if ($transaksi->create($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-header text-center">
                Data Siswa
            </div>
            <div class="card-body">
                <?php foreach ($siswa as $row) : ?>
                    <img src="<?= BASE_URL; ?>/assets/img/undraw_profile.svg" class="img-fluid p-4" alt="">
                    <p>Nis : <?= $row['nis']; ?></p>
                    <p>Nama Siswa : <?= $row['nama']; ?></p>
                    <p>Kelas : <?= $row['kelas']; ?></p>
                    <p>Jurusan : <?= $row['kompetensi_keahlian']; ?></p>
                    <p>Semester : <?= $row['tahun_ajaran']; ?></p>
                    <p>Nominal : <?= $row['nominal']; ?></p>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="row">
            <div class="col">
                <form method="post">
                    <?php foreach ($siswa as $row) : ?>
                        <input type="hidden" name="id_siswa" value="<?= $row['id_siswa']; ?>">
                        <input type="hidden" name="id_pembayaran" value="<?= $row['id_pembayaran']; ?>">
                    <?php endforeach; ?>

                    <div class="row">
                        <?php foreach ($data['data_bulan'] as $bulan) : ?>
                            <div class="col-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <input type="checkbox" name="bulan_bayar[]" value="<?= $bulan[1]; ?>" <?= in_array($bulan[1], $data['bulan_bayar']) ? "checked disabled" : ""; ?>>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?= $bulan[0]; ?>" disable>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">kirim</button>
                </form>
            </div>
        </div>
    </div>
</div>



<?php include '../templates/footer.php' ?>