<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="../../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= BASE_URL ?>/assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Melali<sup>Yuk</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <?php if ($_SESSION['user']['role'] == 1) : ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?= BASE_URL; ?>/views/admin/dashboard.php">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#petugas" aria-expanded="true" aria-controls="petugas">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Petugas</span>
                    </a>
                    <div id="petugas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-petugas/index.php">Data Petugas</a>
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-petugas/create.php">Tambah Petugas</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#siswa" aria-expanded="true" aria-controls="siswa">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Siswa</span>
                    </a>
                    <div id="siswa" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-siswa/index.php">Data Siswa</a>
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-siswa/create.php">Tambah Siswa</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pembayaran" aria-expanded="true" aria-controls="pembayaran">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Pembayaran</span>
                    </a>
                    <div id="pembayaran" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-pembayaran/index.php">Data Pembayaran</a>
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/admin-pembayaran/create.php">Tambah Pembayaran</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#kelas" aria-expanded="true" aria-controls="kelas">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Kelas</span>
                    </a>
                    <div id="kelas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/kelas/index.php">Data Kelas</a>
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/kelas/create.php">Tambah Kelas</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transaksi" aria-expanded="true" aria-controls="transaksi">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Transaksi</span>
                    </a>
                    <div id="transaksi" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/transaksi/index.php">Data Transaksi</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#history" aria-expanded="true" aria-controls="history">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>History</span>
                    </a>
                    <div id="history" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/history/index.php">Data Transaksi</a>
                        </div>
                    </div>
                </li>

            <?php elseif ($_SESSION['user']['role'] == 2) : ?>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#kelas" aria-expanded="true" aria-controls="kelas">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Kelas</span>
                    </a>
                    <div id="kelas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/kelas/index.php">Data Kelas</a>
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/kelas/create.php">Tambah Kelas</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#history" aria-expanded="true" aria-controls="history">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>History</span>
                    </a>
                    <div id="history" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/history/index.php">Data Transaksi</a>
                        </div>
                    </div>
                </li>
            <?php else : ?>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#history" aria-expanded="true" aria-controls="history">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>History</span>
                    </a>
                    <div id="history" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="<?= BASE_URL; ?>/views/history/siswa.php?id_siswa=<?= $_SESSION['id_siswa']; ?>">Data Transaksi</a>
                        </div>
                    </div>
                </li>
            <?php endif; ?>







        </ul>
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['user']['username']; ?></span>
                                <img class="img-profile rounded-circle" src="<?= BASE_URL; ?>/assets/img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">