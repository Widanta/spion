<?php
include '../../functions/connect.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$pembayaran = mysqli_query($conn, "SELECT * FROM pembayaran");


?>

<?php include '../templates/header.php' ?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <tr>
                <th>No</th>
                <th>tahun ajaran</th>
                <th>nominal</th>
            </tr>
            <?php foreach ($pembayaran as $row) : ?>
                <tr>
                    <td>
                        <a href="edit.php?id_pembayaran=<?= $row['id_pembayaran'] ?>" class="btn"><i class="fas fa-pen text-primary"></i></a>
                        <a href="delete.php?id_pembayaran=<?= $row['id_pembayaran'] ?>" class="btn"><i class="fas fa-trash text-danger"></i></a>
                    </td>
                    <td><?= $row['tahun_ajaran'] ?></td>
                    <td><?= $row['nominal'] ?></td>
                <?php endforeach; ?>
        </table>
    </div>
</div>
<?php include '../templates/footer.php' ?>