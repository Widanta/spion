<?php
include '../../functions/pembayaran.php';
$pembayaran = new Pembayaran();

if (isset($_POST['submit'])) {
    if ($pembayaran->create($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <form method="post">
        <div class="form-group">
            <label for="tahun_ajaran">tahun_ajara</label>
            <input type="text" name="tahun_ajaran" class="form-control">
        </div>
        <div class="form-group">
            <label for="nominal">nominal</label>
            <input type="text" name="nominal" class="form-control">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">kirim</button>
    </form>
</div>
<?php include '../templates/footer.php' ?>