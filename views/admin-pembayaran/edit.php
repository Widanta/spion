<?php
include '../../functions/pembayaran.php';
$conn = mysqli_connect('localhost', 'root', '', 'spion');
$pembayaran = new Pembayaran();

if (isset($_POST['submit'])) {
    if ($pembayaran->edit($_POST) > 0) {
        header("Location: index.php");
        exit;
    }
}

$pembayaran = mysqli_query($conn, "SELECT * FROM pembayaran WHERE id_pembayaran = {$_GET['id_pembayaran']} ");

?>

<?php include '../templates/header.php' ?>
<div class="row">

    <?php foreach ($pembayaran as $row) : ?>
        <form method="post">
            <input type="hidden" name="id_pembayaran" value="<?= $row['id_pembayaran']; ?>">
            <div class="form-group">
                <label for="tahun_ajaran">tahun_ajara</label>
                <input type="text" name="tahun_ajaran" class="form-control" value="<?= $row['tahun_ajaran']; ?>">
            </div>
            <div class="form-group">
                <label for="nominal">nominal</label>
                <input type="text" name="nominal" class="form-control" value="<?= $row['nominal']; ?>">
            </div>
            <button type="submit" name="submit" class="btn btn-primary">kirim</button>
        </form>
    <?php endforeach; ?>
</div>
<?php include '../templates/footer.php' ?>