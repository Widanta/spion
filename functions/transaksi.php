<?php
include 'connect.php';
class Transaksi extends Connect
{
    public function create($data)
    {
        $tahun = date('Y');
        $siswa = $data['id_siswa'];
        $pembayaran = $data['id_pembayaran'];
        $petugas = $_SESSION['id_petugas'];
        foreach ($data['bulan_bayar'] as $bulan) {
            $query = "INSERT INTO transaksi VALUES (null, NOW(), '$bulan', '$tahun', '$siswa', '$petugas', '$pembayaran')";
            mysqli_query($this->conn, $query);
        }
        return mysqli_affected_rows($this->conn);
    }
}
