<?php
session_start();

define('BASE_URL', 'http://localhost/spion');

class Connect
{

    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $db = 'spion';

    protected $conn;


    function __construct()
    {
        $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db);
    }

    public function login($username, $password)
    {
        $sql = "SELECT * FROM v_pengguna WHERE username = '$username' AND password = '$password' ";

        $query = mysqli_query($this->conn, $sql);
        $row = $query->num_rows;


        if ($row == 1) {
            $data = $query->fetch_assoc();
            $_SESSION['user'] = [
                'username' => $data['username'],
                'password' => $data['password'],
                'role' => $data['role']
            ];

            // var_dump($data);
            // exit;

            if ($_SESSION['user']['role'] == 1) {
                $_SESSION['id_petugas'] = $data['id_petugas'];
                header("Location: views/admin/dashboard.php");
            } else if ($_SESSION['user']['role'] == 2) {
                header("Location: views/petugas/dashboard.php");
            } else {
                $_SESSION['id_siswa'] = $data['id_siswa'];
                header("Location: views/siswa/dashboard.php");
            }
        }
    }
}
