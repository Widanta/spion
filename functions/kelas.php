<?php
include 'connect.php';


class Kelas extends Connect
{
    public function create($data)
    {
        $nama = $data['kelas'];
        $komp = $data['kompetensi_keahlian'];

        $query = "INSERT INTO kelas VALUES (null, '$nama', '$komp')";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }

    public function edit($data)
    {
        $nama = $data['kelas'];
        $komp = $data['kompetensi_keahlian'];
        $id_kelas = $data['id_kelas'];

        $query = "UPDATE kelas SET kelas = '$nama', kompetensi_keahlian = '$komp' WHERE id_kelas = '$id_kelas'";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }

    public function delete($data)
    {
        $id_kelas = $data['id_kelas'];

        $query = "DELETE FROM kelas WHERE id_kelas = '$id_kelas' ";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }
}
