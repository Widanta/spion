<?php

class Laporan
{
    public $tahun;

    public function __construct($tahun)
    {
        $this->tahun = $tahun;
    }

    public function totalPemasukanBulan($angkaBulan)
    {
        global $db;
        $sql = mysqli_query($db, "SELECT SUM(nominal) FROM transaksi INNER JOIN pembayaran 
        ON transaksi.id_pembayaran = pembayaran.id_pembayaran 
        WHERE bulan_bayar = '$angkaBulan' 
        AND tahun_ajaran = '$this->tahun' ");

        $total_pemasukan = mysqli_fetch_array($sql, MYSQLI_NUM);
        return $total_pemasukan[0];
    }
}
