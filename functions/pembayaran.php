<?php
include 'connect.php';


class Pembayaran extends Connect
{
    public function create($data)
    {
        $tahun = $data['tahun_ajaran'];
        $nominal = $data['nominal'];

        $query = "INSERT INTO pembayaran VALUES (null, '$tahun', '$nominal')";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }

    public function edit($data)
    {
        $tahun = $data['tahun_ajaran'];
        $nominal = $data['nominal'];
        $id_pembayaran = $data['id_pembayaran'];

        $query = "UPDATE pembayaran SET tahun_ajaran = '$tahun', nominal = '$nominal' WHERE id_pembayaran = '$id_pembayaran'";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }

    public function delete($data)
    {
        $id_pembayaran = $data['id_pembayaran'];

        $query = "DELETE FROM pembayaran WHERE id_pembayaran = '$id_pembayaran' ";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }
}
