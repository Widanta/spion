<?php
include 'connect.php';


class Siswa extends Connect
{
    public function create($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $role = $data['role'];

        $query = "INSERT INTO pengguna VALUES (null, '$username', '$password',  '$role')";
        $pengguna = mysqli_query($this->conn, $query);

        if ($pengguna) {
            $id_pengguna = $this->conn->insert_id;
            $nisn = $data['nisn'];
            $nis = $data['nis'];
            $nama = $data['nama'];
            $alamat = $data['alamat'];
            $telepon = $data['telepon'];
            $kelas = $data['id_kelas'];
            $pembayaran = $data['id_pembayaran'];
            $query = "INSERT INTO siswa VALUES (null, '$nisn', '$nis', '$nama', '$alamat', '$telepon', '$kelas', '$id_pengguna' , '$pembayaran')";
            mysqli_query($this->conn, $query);
            return mysqli_affected_rows($this->conn);
        }
    }

    public function edit($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $role = $data['role'];
        $id_pengguna = $data['id_pengguna'];

        $query = "UPDATE pengguna SET username = '$username', password = '$password', role = '$role' WHERE id_pengguna = '$id_pengguna'";
        $pengguna = mysqli_query($this->conn, $query);
        if ($pengguna) {
            $nisn = $data['nisn'];
            $nis = $data['nis'];
            $nama = $data['nama'];
            $alamat = $data['alamat'];
            $telepon = $data['telepon'];
            $kelas = $data['id_kelas'];
            $pembayaran = $data['id_pembayaran'];
            $id_siswa = $data['id_siswa'];
            $query = "UPDATE siswa SET nisn = '$nisn',  nis = '$nis',  nama = '$nama',  alamat = '$alamat',  telepon = '$telepon',id_kelas = '$kelas', id_pengguna = '$id_pengguna', id_pembayaran = '$pembayaran' WHERE id_siswa = '$id_siswa'";
            mysqli_query($this->conn, $query);
            return mysqli_affected_rows($this->conn);
        }
    }

    public function delete($data)
    {
        $id_pengguna = $data['id_pengguna'];

        $query = "DELETE FROM pengguna WHERE id_pengguna = '$id_pengguna' ";
        mysqli_query($this->conn, $query);
        return mysqli_affected_rows($this->conn);
    }
}
